import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent {

  settings = {
    columns: {
      id: {
        title: 'Ticket ID',
      },
      ticketName: {
        title: 'Ticket Name',
      },
      reporter: {
        title: 'Reporter',
      },
      createdDate: {
        title: 'Create Date',
      },
      status: {
        title: 'Status',
      },
      activity: {
        title: 'Activity',
      },
      productStream: {
        title: 'Product Stream'
      }
    },
  };

  datas = [
    {
      id: "ABC123",
      ticketName: "Bronet Axis",
      reporter: "Ikhsan",
      createdDate: "2020-09-04",
      status:"success",
      activity:"Initial Ticket",
      productStream:"AXIS",
    },
    {
      id: "ABC234",
      ticketName: "Sisnet Axis",
      reporter: "Sunarto",
      createdDate: "2020-06-01",
      status:"success",
      activity:"Approved by BRM",
      productStream:"PREPAID",
    },
    {
      id: "ABC345",
      ticketName: "Internet Axis",
      reporter: "Sunardi",
      createdDate: "2020-09-02",
      status:"warning",
      activity:"Waiting Approval",
      productStream:"POSTPAId",
    },
    {
      id: "ABC456",
      ticketName: "Gamers Package",
      reporter: "Sunjaya",
      createdDate: "2020-07-19",
      status:"danger",
      activity:"Withdrew by BRM",
      productStream:"VAS",
    },
    {
      id: "ABC567",
      ticketName: "Paket Mantep",
      reporter: "Ikhsan",
      createdDate: "2020-08-22",
      status:"success",
      activity:"Approved by Tester",
      productStream:"AXIS",
    },
  ];

}
